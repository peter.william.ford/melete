import melete.core.mtime as m
import melete.core.timeframe as tf
from melete.core.event import EventType, EventHolder
import pytest
from pytest import lazy_fixture as lf
from pytest import approx as appr
from math import floor, ceil

@pytest.fixture
def c():
    return m.SemibreveFraction(4)

@pytest.fixture
def q():
    return m.SemibreveFraction(8)

@pytest.fixture
def tempo120(c):
    return m.Tempo(120, c)

@pytest.fixture
def three4(c):
    return m.TimeSignature(3, c)

@pytest.fixture
def four4(c):
    return m.TimeSignature(4, c)

class TestTimeFrame:
    def test_TimeFrame(self, c, tempo120, three4, four4):
        tf1 = tf.TimeFrame.from_tempo_and_ts(
            tempo120, three4, 5, 1.0)
        print(tf1.bars())
        print(tf1.bar_starts())

        assert len(tf1.bars()) == 2
        assert len(tf1.bar_starts()) == 2
        assert len(tf1.bars()[0]) == 3
        assert len(tf1.bars()[1]) == 2
        assert tf1.avg_duration() == appr(0.5)

        assert tf1.ts_of_bar(0) == three4
        assert tf1.ts_of_bar(1) == m.TimeSignature(2, c)

        for t in [x/4 for x in range(0, 16)]:
            b1, i1 = tf1.beat_before(t)
            b2, i2 = tf1.beat_after(t)
            #print('Before time {}: b={}, i={}'.format(t, b1, i1))
            #print('After time {}: b={}, i={}\n'.format(t, b2, i2))
            if t < tf1.beats[0].t:
                assert b1 is None and i1 is None
                assert b2 == tf1.beats[0]
                assert i2 == 0
            elif t >= tf1.beats[-1].t:
                assert b1 == tf1.beats[-1]
                assert i1 == len(tf1.beats) - 1
                assert b2 is None and i2 is None
            else:
                assert i1 == floor((t - 1.0) * 2)
                assert tf1.beats[i1] == b1
                assert i2 == ceil((t - 1.0 + m._t_eps) * 2)
                assert tf1.beats[i2] == b2

        for t in [x*0.000111 for x in range(0, 36000)]:
            b, i = tf1.beat_closest(t)
            if t <= tf1.beats[0].t + 0.25:
                assert b == tf1.beats[0]
                assert i == 0
            elif t >= tf1.beats[-1].t - 0.25:
                assert b == tf1.beats[-1]
                assert i == len(tf1.beats) - 1
            else:
                assert i == round((t - 1.0) * 2)
                assert tf1.beats[i] == b

        beats = tf1.beats
        beats.pop(0)
        tf2 = tf.TimeFrame(beats, c)
        print(tf2.bar_starts())
        print(tf2.bars())

        assert len(tf2.bars()) == 2
        assert len(tf2.bar_starts()) == 2
        assert len(tf2.bars()[0]) == 2
        assert len(tf2.bars()[1]) == 2

        events = [
            EventHolder(m.cTime(0, 0, 0.0),
                           tempo120,
                           EventType.tempo),
            EventHolder(m.cTime(2, 0, 4.0),
                        m.Rit(m.Tempo(60.0, c),
                              m.Tempo(40.0, c),
                              m.bDur(5.5, four4)),
                              EventType.rit)
        ]
        stf = tf.StructuredTimeFrame.from_events(four4, events, tf.bTime(5, 0))

        print(stf)
        print(stf.bar_starts())
        print(stf.bars())

        assert len(stf.events) == 2
        assert len(stf.beats) == 21
        assert stf.beats[-1] == tf.cTime(5, 0, 20.440174)

    def test_get_time(self, c, tempo120, four4):
        tf1 = tf.TimeFrame.from_tempo_and_ts(
            tempo120, four4, 15, 4.0)

        print(tf1)

        for t in [m.fTime(x * 0.0111) for x in range(0, 360)]:
            if t < tf1.start() or t > tf1.end() + m.fDur(tf1.avg_duration() - m._t_eps):
                with pytest.raises(ValueError):
                    cT, i = tf1.get_cTime(t)
            else:
                cT, i = tf1.get_cTime(t)
                assert t.t == cT.t
                assert t.t >= tf1.beats[i].t
                if i < len(tf1.beats) - 1:
                    assert t.t < tf1.beats[i+1].t
                else:
                    assert t.t < tf1.beats[i].t + tf1.avg_duration()
                assert 0.5 * floor(t.t * 2) == appr(tf1.beats[i].t)
                assert tf1.beats[i].bar == cT.bar
                assert tf1.beats[i].beat == appr(floor(cT.beat))
                #print('Time={}, i={}. cTime={}'.format(t, i, cT))


    def test_get_qTime(self, c, tempo120, four4):
        tf1 = tf.TimeFrame.from_tempo_and_ts(
            tempo120, four4, 15, 10.0)

        print(tf1)
        for t in [m.fTime(9 + x * 0.0111) for x in range(0, 1500)]:
            if t < tf1.start() or t > tf1.end() + m.fDur(tf1.avg_duration() - 1/8):
                with pytest.raises(ValueError):
                    qT, i = tf1.get_best_qTime(t)
            else:
                qT, i = tf1.get_best_qTime(t)
                #print('For t={}, i={}, qT={}'.format(t, i, qT))
                assert qT.qbeat.real() == appr(qT.beat)
                assert abs(tf1.start().t + 0.5*(qT.qbeat.real() + qT.bar * 4) - t.t) <= 0.5 * 1/16