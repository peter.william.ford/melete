import melete.core.mtime as m
import pytest
from pytest import lazy_fixture as lf
from pytest import approx as appr

@pytest.fixture
def c():
    return m.SemibreveFraction(4)

@pytest.fixture
def q():
    return m.SemibreveFraction(8)

@pytest.fixture
def temp100(c):
    return m.Tempo(100, c)

@pytest.fixture
def temp120(c):
    return m.Tempo(120, c)

@pytest.fixture
def temp60(c):
    return m.Tempo(60, c)

@pytest.fixture
def three4(c):
    return m.TimeSignature(3, c)

@pytest.fixture
def four4(c):
    return m.TimeSignature(4, c)

@pytest.fixture
def three8(q):
    return m.TimeSignature(3, q)

@pytest.fixture
def b6f(three4):
    return  m.bDur(6 - 1e-7, three4)

@pytest.fixture
def b6(three4):
    return m.bDur(6, three4)

@pytest.fixture
def b59(three4):
    return  m.bDur(5.9, three4)

@pytest.fixture
def b51(three4):
    return  m.bDur(5.1, three4)

@pytest.fixture
def b6q(three8):
    return m.bDur(6.0, three8)

@pytest.fixture
def b8(three4):
    return  m.bDur(8, three4)

@pytest.fixture
def c6(b6, temp120):
    return m.cDur.from_bdur_and_tempo(b6, temp120)

@pytest.fixture
def c8(b8, temp120):
    return m.cDur.from_bdur_and_tempo(b8, temp120)

@pytest.fixture
def q315():
    return m.qTime(3, m.Frac(1, 1, 2), 6.75)


def test_SemibreveFraction():
    with pytest.raises(ValueError):
        m.SemibreveFraction(3)

    with pytest.raises(ValueError):
        m.SemibreveFraction(0)

    with pytest.raises(ValueError):
        m.SemibreveFraction(3.4)

    q = m.SemibreveFraction(4)
    assert q.name() == 'crotchet'

    q2 = m.SemibreveFraction(4.0)
    assert q == q2

    h = m.SemibreveFraction(128)
    assert str(h) == 'hemihemidemisemiquaver'


def test_TimeSignature(c, q):
    with pytest.raises(TypeError):
        m.TimeSignature(3, 4)

    three = m.TimeSignature(3, c)
    assert len(three.secondaries()) == 0
    print(three)

    six = m.TimeSignature(6, c)
    assert six.secondaries() == [3]

    assert three != six

    threeB = m.TimeSignature.from_str('3/4')
    assert three == threeB

    with pytest.raises(ValueError):
        m.TimeSignature.from_str('34')

    with pytest.raises(ValueError):
        m.TimeSignature.from_str('34')

    with pytest.raises(ValueError):
        m.TimeSignature.from_str('34')


    assert six != m.TimeSignature(6, q)

    assert m.TimeSignature(4, q).secondaries() == [2]

    assert m.TimeSignature(15, c).secondaries() == [3, 6, 9, 12]



class TestTempo:
    @pytest.mark.parametrize("l", [[1], [1, 2, 3], [120, '4'], [120, '4 pm ']])
    def test_from_list_Value_fail(self, l):
        with pytest.raises(ValueError):
            m.Tempo.from_list(l)

    @pytest.mark.parametrize("l", [[1, 2]])
    def test_from_list_Type_fail(self, l):
        with pytest.raises(TypeError):
            m.Tempo.from_list(l)

    @pytest.mark.parametrize("s, bps, std", [('120 4pm', 2.0, 2.0), ('60 8pm', 1.0, 8.0)])
    def test_from_str(self, s, bps, std):
        t = m.Tempo.from_str(s)
        assert t.bps() == bps
        assert t.semibreve_time_delta() == std
        print(t)

    @pytest.fixture
    def temp120f(self, c):
        return m.Tempo(120.0, c)

    @pytest.fixture
    def temp240q(self, q):
        return m.Tempo(240, q)

    @pytest.fixture
    def tempos(self, temp120, temp120f, temp240q):
        return temp120, temp120f, temp240q

    @pytest.mark.parametrize("t, bps", [(lf('temp120'), 2.0),
                                        (lf('temp120f'), 2.0),
                                        (lf('temp240q'), 4.0)])
    def test_bps(self, t, bps):
        assert t.bps() == bps

    @pytest.mark.parametrize("t, delta", [(lf('temp120'),  0.5),
                                          (lf('temp120f'), 0.5),
                                          (lf('temp240q'), 0.25)])
    def test_beat_time_delta(self, t, delta):
        assert t.beat_time_delta() == delta

    @pytest.mark.parametrize("t, delta", [(lf('temp120'),  2.0),
                                          (lf('temp120f'), 2.0),
                                          (lf('temp240q'), 2.0)])
    def test_semibreve_time_delta(self, t, delta):
        assert t.semibreve_time_delta() == delta


    def test_equiv(self, c, q, tempos):
        with pytest.raises(TypeError):
            m.Tempo(120, 4)

        h, hf, h2 = tempos
        assert h == hf
        assert str(h) != str(hf)
        assert h != h2
        assert h.equiv(h2)

        with pytest.raises(TypeError):
            assert not h.equiv(100)


class TestbDur:
    @pytest.mark.parametrize("b, sd, td", [(lf('b6f'), 1.5,   3.0),
                                           (lf('b6'),  1.5,   3.0),
                                           (lf('b6q'), 0.75,  1.5),
                                           (lf('b59'), 1.475, 2.95),
                                           (lf('b51'), 1.275, 2.55)])
    def test_dur(self, b, sd, td, temp120):
        assert b.semibreves_dur() == appr(sd)
        assert b.time_dur(temp120) == appr(td)

    @pytest.mark.parametrize("s", ['1:3', '13', '1:3:2', '1:2.a', '1:-1', '1:a'])
    def test_from_str_fail(self, s, three4):
        with pytest.raises(ValueError):
            m.bDur.from_str(s, three4)

    def test_bDur_create(self, three4, b51, b6):
        with pytest.raises(TypeError):
            m.bDur(1, 4)

        b51s = m.bDur.from_str('1:2.100', three4)
        assert b51s == b51
        print(b51s)

        b6s = m.bDur.from_str('2:0', three4)
        assert b6 == b6s
        print(b6)

    def test_bDur_arith(self, three4, b6f, b6, b59, b51):
        assert b6f.is_integer
        assert b6.is_integer
        assert not b59.is_integer
        assert not b51.is_integer

        assert b6.bars == 2
        assert b6f.bars == 2
        assert b59.bars == 1
        assert b59.bars == 1

        assert b6f.beats_proper == appr(-1e-7)
        assert b6.beats_proper ==  appr(0)
        assert b59.beats_proper == appr(2.9)
        assert b51.beats_proper == appr(2.1)

        assert b6 != b59
        assert b6 == b6f
        assert b6f + b6f == m.bDur(12, three4)
        assert b6 - b59 == m.bDur(0.1, three4)
        assert b6 <= b6f
        assert b59 <= b6
        assert b59 < b6
        assert not b6 < b6f

    def test_cDur(self, b6, c):
        T = m.Tempo(120, c)
        c1 = m.cDur.from_bdur_and_tempo(b6, T)
        assert c1.get_bDur() == b6
        assert c1.time_dur(T) == appr(c1.secs)
        assert c1.secs == appr(3.0)

        c2 = m.cDur.from_bdur_and_fdur(b6, m.fDur(3.0))
        assert c1 == c2

class TestbcTime:
    def test_bTime(self, b6, b8, three4):
        t10 = m.bTime(1, 0)
        t10a = m.bTime(1, 0)
        t12 = m.bTime(1, 2)

        print(t10)
        print(t12)

        assert t10 == t10a
        assert t10 <= t10a
        assert not t10 < t10a

        assert t10 < t12
        assert not t10 == t12

        assert m.bTime(3, 0) == t10 + b6
        assert m.bTime(3, 2) == t12 + b6
        assert m.bTime(3, 2) == t10 + b8
        assert m.bTime(3, 4) != t12 + b8
        assert m.bTime(4, 1) == t12 + b8
        assert b8 ==  m.bTime(3,2).sub_using_TS(t10, three4)

    def test_cTime(self, c6, c8, temp120, three4):
        t10 = m.cTime(1, 0, 1.5)
        t10a = m.cTime(1, 0, 1.5)
        t10b = m.cTime(1, 0, 2.0)
        t12 = m.cTime(1, 2, 2.5)

        assert t10 == t10a
        assert t10 <= t10a
        assert not t10 < t10a

        assert t10 != t10b

        assert t10 < t12
        assert not t10 == t12

        assert m.cTime(3, 0, 4.5) == t10 + c6
        assert m.cTime(3, 0, 4.5) == t10.plus_dur_at_tempo(c6, temp120)
        assert m.cTime(3, 2, 5.5) == t12 + c6
        assert m.cTime(3, 2, 5.5) == t12.plus_dur_at_tempo(c6, temp120)
        assert m.cTime(3, 2, 5.5) == t10 + c8
        assert m.cTime(3, 2, 5.5) == t10.plus_dur_at_tempo(c8, temp120)
        assert m.cTime(3, 4, 6.5) != t12 + c8
        assert m.cTime(3, 4, 6.5) != t12.plus_dur_at_tempo(c8, temp120)
        assert m.cTime(4, 1, 6.5) == t12 + c8
        assert m.cTime(4, 1, 6.5) == t12.plus_dur_at_tempo(c8, temp120)
        assert c8.get_bDur() ==  m.bTime(3, 2).sub_using_TS(t10, three4)

    def test_pause(self):
        t1 = m.cTime(1, 0, 2)
        t2 = m.cTime(1, 0, 3)
        t3 = m.cTime(1, 1, 4)
        with pytest.raises(ValueError):
            m.Pause(t1, t3)

        p = m.Pause(t1, t2)
        assert p.dur() == appr(1.0)

class TestFrac:
    def test_creation(self):
        assert m.Frac(1, 2, 4).N == 1
        assert m.Frac(1, 2, 4).D == 2
        assert m.Frac(1, 2, 4).real() == appr(1.5)

        with pytest.raises(TypeError):
            m.Frac(1, 2.5, 4)
        with pytest.raises(ValueError):
            m.Frac(-1, 2, 4)
        with pytest.raises(ValueError):
            m.Frac(1, 4, 4)
        with pytest.raises(ValueError):
            m.Frac(1, -1, 4)
        with pytest.raises(ValueError):
            m.Frac(1, 4, -5)

        assert m.Frac(1, 2, 4) == m.Frac(1, 2, 4)

        assert m.Frac.fixed_D(1.8, 4) == m.Frac(1, 3, 4)
        assert m.Frac.fixed_D(1.1, 4) == m.Frac(1, 0, 1)
        assert m.Frac.fixed_D(9.2, 6) == m.Frac(9, 1, 6)
        assert m.Frac.D_set(9.2) == m.Frac(9, 1, 4)
        assert m.Frac.D_set(9.2, [1, 2, 3, 4, 6, 8]) == m.Frac(9, 1, 6)

    def test_arith(self):
        s = m.Frac(0, 2, 3) + m.Frac(0, 1, 2)
        assert s.real() == appr(1 + 1/6)
        assert s == m.Frac(1, 1, 6)
        print(s)

        assert m.Frac(0, 4, 5) - m.Frac(0, 1, 2) == m.Frac(0, 3, 10)

        assert m.Frac(1, 1, 3) * 3 == m.Frac(4, 0, 1)
        assert m.Frac(1, 1, 3) / 2 == m.Frac(0, 2, 3)
        assert m.Frac(1, 1, 3) * m.Frac(3, 0, 1) == m.Frac(4, 0, 1)
        assert m.Frac(1, 1, 2) * m.Frac(1, 1, 3) == m.Frac(2, 0, 1)
        assert m.Frac(4, 1, 3) - 3 == m.Frac(1, 1, 3)
        with pytest.raises(NotImplementedError):
            m.Frac(1, 1, 3) + 2.5

class TestqTime:
    def test_ops(self, four4, q315):
        c315 = m.cTime(3, 1.5, 6.75)
        assert c315 == q315
        assert q315 == q315
        assert q315.beat == appr(c315.beat)
        assert q315 == m.qTime.from_cTime(c315, four4)

        q3378 = m.qTime(3, m.Frac(3, 7, 8), 15.875)
        q4010 = m.qTime(4, m.Frac(0, 0, 1), 16.0)

        assert q3378.get_bTime() == m.qTime.from_cTime(
            m.cTime(3, 3.93, 15.93), four4).get_bTime()
        assert q4010.get_bTime() == m.qTime.from_cTime(m.cTime(3, 3.97, 15.97), four4).get_bTime()

        q132 = m.qTime(1, m.Frac(3, 2, 3), 3.8333333333333)
        d = q315.sub_using_TS(q132, four4)
        assert d == m.qDur(m.Frac(5, 5, 6), 2.916666666667, four4)

    def test_rel_fns(self, four4, q315, temp120):
        assert q315.next_beat(four4, temp120) == m.qTime(3, m.Frac(2, 0, 1), 7)
        q31 = m.qTime(3, m.Frac(1, 0, 1), 6.5)
        q32 = m.qTime(3, m.Frac(2, 0, 1), 7)
        assert q31.next_beat(four4, temp120) == q32

        assert q31 + m.qDur(m.Frac(0, 1, 2), 0.25, four4) == q315
        assert (q31 + m.qDur(m.Frac(0, 2, 3), 0.333333333333333333333, four4)).next_beat(four4, temp120) == q32
        assert (q31 + m.qDur(m.Frac(0, 2, 3), 0.0, four4)).next_beat(four4, temp120) != q32

    def test_from_ctime(self, four4, q315, temp120):
        #TODO!!
        pass

class TestQStartEnd:
    def test_arith(self, four4, temp120, q315):
        with pytest.raises(ValueError):
            m.qStartEnd(q315, q315)

        se = m.qStartEnd(m.qTime(0, m.Frac(2, 0, 1), 1.0), m.qTime(1, m.Frac(3, 1, 2), 3.75))
        assert se.dur_at_one_TS(four4) == m.qDur(m.Frac(5, 1, 2), 2.75, four4)
        assert se.implied_average_tempo(four4) == temp120

    def test_fragments(self, four4, temp120, q315):
        se1 = m.qStartEnd(q315, m.qTime(3, m.Frac(1, 3, 4), 6.875))
        df1 = se1.get_duration_fragments(four4)
        assert len(df1) == 1
        assert df1[0] == se1.dur_at_one_TS(four4)
        assert df1[0] == m.qDur(m.Frac(0, 1, 4), 0.125, four4)

        with pytest.raises(ValueError):
            m.qStartEnd(m.qTime(1, m.Frac(0,0,1), 0), m.qTime(0, m.Frac(0,0,1), 0)).get_duration_fragments(four4)

        se2 = m.qStartEnd(q315, m.qTime(3, m.Frac(2, 3, 4), 7.375))
        df2 = se2.get_duration_fragments(four4)
        assert len(df2) == 2
        assert df2[0] == m.qDur(m.Frac(0, 1, 2), 0.25, four4)
        assert df2[1] == m.qDur(m.Frac(0, 3, 4), 0.375, four4)

        se3 = m.qStartEnd(q315, m.qTime(4, m.Frac(1, 1, 2), 8.75))
        df3 = se3.get_duration_fragments(four4)
        assert len(df3) == 5
        assert df3[0] == m.qDur(m.Frac(0, 1, 2), 0.25, four4)
        assert df3[1] == m.qDur(m.Frac(1, 0, 1), 0.5, four4)
        assert df3[2] == m.qDur(m.Frac(1, 0, 1), 0.5, four4)
        assert df3[3] == m.qDur(m.Frac(1, 0, 1), 0.5, four4)
        assert df3[4] == m.qDur(m.Frac(0, 1, 2), 0.25, four4)



class TestRit:
    def test_rits(self, temp120, temp60, b6):
        faster = m.Rit(temp60, temp120, b6)
        slower = m.Rit(temp120, temp60, b6)
        slow = m.Rit(temp60, temp60, b6)
        fast = m.Rit(temp120, temp120, b6)
        lbound = m.cDur.from_bdur_and_tempo(b6, temp120)
        ubound = m.cDur.from_bdur_and_tempo(b6, temp60)

        assert faster.total_cDur().get_bDur() == b6
        assert faster.total_cDur().secs < ubound.secs
        assert faster.total_cDur().secs > lbound.secs

        assert slower.total_cDur().get_bDur() == b6
        assert slower.total_cDur().secs < ubound.secs
        assert slower.total_cDur().secs > lbound.secs

        assert fast.total_cDur() == lbound

        assert slow.total_cDur() == ubound

    def test_fixed_rits(self, temp120, temp60, b6):
        start = m.cTime(1, 0, 1.5)
        faster = m.FixedRit(start, temp60, temp120, b6)
        slower = m.FixedRit(start, temp120, temp60, b6)
        slow =   m.FixedRit(start, temp60, temp60, b6)
        fast =   m.FixedRit(start, temp120, temp120, b6)
        lbound = start + m.cDur.from_bdur_and_tempo(b6, temp120)
        ubound = start + m.cDur.from_bdur_and_tempo(b6, temp60)

        assert faster.end().get_bTime() == start.get_bTime() + b6
        assert faster.end().t <  ubound.t
        assert faster.end().t >  lbound.t

        assert slower.end().get_bTime() == start.get_bTime() + b6
        assert slower.end().t <  ubound.t
        assert slower.end().t >  lbound.t

        assert fast.end() == lbound

        assert slow.end() == ubound
