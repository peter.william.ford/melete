import melete.core.pitch as p
import pytest
from pytest import lazy_fixture as lf
from pytest import approx as appr

@pytest.fixture
def C():
    return p.NNN['C']

@pytest.fixture
def D():
    return p.NNN['D']

@pytest.fixture
def B():
    return p.NNN['B']

@pytest.fixture
def CSharp(C):
    return p.NoteName(C, 1)

@pytest.fixture
def DFlat(D):
    return p.NoteName(D, -1)

@pytest.fixture
def CMajor(C):
    return p.Key(p.NoteName(C))

@pytest.fixture
def CMinor(C):
    return p.MinorKey(p.NoteName(C))


def test_NNN(C):
    assert len(p.NNN.values()) == 7
    assert p.NNN.values()[-1] == 11
    assert p.NNN.names()[-1] == 'B'
    assert p.NNN.from_degree(9) == p.NNN['E']
    assert p.NNN['B'].next_up() == p.NNN['C']
    assert p.NNN['A'].next_down() == p.NNN['G']
    assert p.NNN['A'] == p.NNN['A']
    assert C == C

def test_NoteName(B, C, D, CSharp, DFlat):
    with pytest.raises(TypeError):
        p.NoteName('C', 0)

    assert p.NoteName(C) == p.NoteName(C, 0)
    assert CSharp != DFlat
    assert CSharp.semitone == DFlat.semitone

    assert p.NoteName.from_semitone(1, True) == CSharp
    assert p.NoteName.from_semitone(1, False) == DFlat
    assert p.NoteName.from_semitone_and_NNN(1, C) == CSharp
    assert p.NoteName.from_semitone_and_NNN(1, B) == p.NoteName(B, 2)

    assert p.NoteName.from_str('C#') == CSharp
    assert p.NoteName.from_str('Df') == DFlat
    assert p.NoteName.from_str('Dfff') == p.NoteName(D, -3)
    bss = p.NoteName.from_str('B###')
    bss.normalise()
    assert bss == p.NoteName(C, 2)

def test_Key(B, C, D, CSharp, DFlat, CMajor, CMinor):
    with pytest.raises(TypeError):
        p.Key(C)

    with pytest.raises(TypeError):
        p.MinorKey(C)

    assert len(CMajor.keySignature()) == 7
    assert len(CMinor.keySignature()) == 7
    assert len(CMajor.melodicNotes()) == 0
    assert len(CMinor.melodicNotes()) == 2

    assert CMajor == CMajor
    assert CMinor == CMinor
    assert CMinor != CMajor
    assert CMajor != CMinor


@pytest.mark.parametrize("st, nn, shift", [(0, p.NNN['C'], 0),
                                           (1, p.NNN['D'], -1),
                                           (2, p.NNN['D'], 0),
                                           (3, p.NNN['E'], -1),
                                           (4, p.NNN['E'], 0),
                                           (5, p.NNN['F'], 0),
                                           (6, p.NNN['F'], 1),
                                           (7, p.NNN['G'], 0),
                                           (8, p.NNN['A'], -1),
                                           (9, p.NNN['A'], 0),
                                           (10, p.NNN['B'], -1),
                                           (11, p.NNN['B'], 0)
                                           ])
def test_Key_best_note_major(CMajor, CMinor, st, nn, shift):
    assert CMajor.bestNoteName(st) == p.NoteName(nn, shift)
    assert CMinor.bestNoteName(st) == p.NoteName(nn, shift)


def test_Pitch(C, CSharp, DFlat):
    with pytest.raises(TypeError):
        p.Pitch(0, C)

    with pytest.raises(ValueError):
        p.Pitch(12, CSharp)

    assert p.Pitch(25, CSharp) != p.Pitch(25, DFlat)

@pytest.mark.parametrize("mn, nn_str", [(12, 'C'), (13, 'C#'), (14, 'D'), (15, 'Ef'),
                                        (16, 'E'), (17, 'F'), (18, 'F#'), (19, 'G'),
                                        (20, 'G#'), (21, 'A'), (22, 'Bf'), (23, 'B')])
def test_Pitch_octave(mn, nn_str):
    for i in range(-2, 10):
        nn = p.NoteName.from_str(nn_str)
        n = p.Pitch(mn + 12 * i, nn)
        assert n.octave == i
