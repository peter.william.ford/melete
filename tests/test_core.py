import melete.core.timeframe as tf

def test_make_timeframes():
    q = tf.SemibreveFraction(4)
    tf1 = tf.TimeFrame.from_tempo_and_ts(
        tf.Tempo(120.0, q), tf.TimeSignature(3, q), 5, 1.0)
    print(tf1.bars())
    print(tf1.bar_starts())

    assert len(tf1.bars()) == 2
    assert len(tf1.bar_starts()) == 2
    assert len(tf1.bars()[0]) == 3
    assert len(tf1.bars()[1]) == 2

    b, i = tf1.beat_before(2.5)
    print('beat: {}, i: {}'.format(b, i))


    beats = tf1.beats
    beats.pop(0)
    tf2 = tf.TimeFrame(beats, q)
    print(tf2.bar_starts())
    print(tf2.bars())

    assert len(tf2.bars()) == 2
    assert len(tf2.bar_starts()) == 2
    assert len(tf2.bars()[0]) == 2
    assert len(tf2.bars()[1]) == 2

    events = [
        tf.EventHolder(tf.cTime(0, 0, 0.0),
                       tf.Tempo(120.0, q),
                       tf.EventType.tempo),
        tf.EventHolder(tf.cTime(2, 0, 4.0),
                       tf.Rit(tf.Tempo(60.0, q),
                              tf.Tempo(40.0, q),
                              tf.bDur(5.5, tf.TimeSignature(4, q))),
                              tf.EventType.rit)
    ]
    stf = tf.StructuredTimeFrame.from_events(tf.TimeSignature(4, q), events, tf.bTime(5, 0))

    print(stf)
    print(stf.bar_starts())
    print(stf.bars())

    assert len(stf.events) == 2
    assert len(stf.beats) == 21
    assert stf.beats[-1] == tf.cTime(5, 0, 20.440174)

if __name__ == '__main__':
    test_make_timeframes()
