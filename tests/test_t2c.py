import melete.t2c.writer as writer
import melete.t2c.reader as reader
from melete.core.mtime import Tempo, SemibreveFraction, TimeSignature, cTime

import argparse

def read_write(infile, outfile, verbosity):
    cmds, state = reader.t2cReader(infile, verbosity)
    print('Final state: {}'.format(state))

    writer.t2cWriter(cmds, outfile)

def test_read_write():
    infile = 'tests/demo.t2c'
    outfile = 'tests/demo_copy.t2c'
    verbosity = 2

    cmds, state = reader.t2cReader(infile, verbosity)
    print('Final state: {}'.format(state))
    quarter = SemibreveFraction(4)
    assert state.tempo == Tempo(40.0, quarter)
    assert state.ts == TimeSignature(4, quarter)
    assert state.rit is None
    assert state.time == cTime(5, 0, 19.514767)

    writer.t2cWriter(cmds, outfile)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=str, help='Input .t2c file')
    parser.add_argument('outfile', type=str, help='Output .t2c file')
    parser.add_argument('verbosity', type=int,
                        help='Verbosity: 0=Warnings, 1=Verbose, 2=Debug')

    SETTINGS, unparsed = parser.parse_known_args()
    read_write(SETTINGS.infile, SETTINGS.outfile, SETTINGS.verbosity)


