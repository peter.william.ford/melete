import melete.core.mtime as m
import melete.core.timeframe as tf
import melete.core.pitch as p
import melete.core.note as n
from melete.core.event import EventType, EventHolder
import melete.core.voice as v

import pytest
from pytest import lazy_fixture as lf
from pytest import approx as appr
from math import floor, ceil

@pytest.fixture
def c():
    return m.SemibreveFraction(4)

@pytest.fixture
def C():
    return p.Pitch(24, p.NoteName(p.NNN['C'], 0))

@pytest.fixture
def q():
    return m.SemibreveFraction(8)

@pytest.fixture
def tempo120(c):
    return m.Tempo(120, c)

@pytest.fixture
def three4(c):
    return m.TimeSignature(3, c)

@pytest.fixture
def four4(c):
    return m.TimeSignature(4, c)


@pytest.fixture
def stf44(tempo120, four4):
    events = [
        EventHolder(m.cTime(0, 0, 0.0),
                    tempo120,
                    EventType.tempo)
    ]
    return tf.StructuredTimeFrame.from_events(four4, events, tf.bTime(5, 0))


class TestStructuredQVoice:
    def test_rest_adding(self, stf44, C):
        b = stf44.beats

        v0 = v.structuredqVoice(
            b[3], [n.qNote(C, b[4], b[5]), n.qNote(C, b[5], b[6]), n.qNote(C, b[7], b[8]), n.qNote(C, b[8], b[9])], stf44)

        assert len(v0.notes) == 6
