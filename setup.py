from setuptools import setup, find_packages

setup(name="melete",
      version='0.1',
      author='Peter Ford',
      author_email='peter.william.ford@gmail.com',
      packages=find_packages(where="src"),
      package_dir={"": "src"},
      )
