from melete.core.mtime import cTime, bDur
from melete.core.timeframe import timeState
from typing import List
from enum import Enum
import sys

class CBP:
    def __init__(self, cbp: int):
        self.cbp = int(cbp)

    def __str__(self):
        return '{} clicks per beat'.format(self.cbp)


#TODO: A Click probably becomes a special case of Note?
class Click:
    def __init__(self, t: cTime):
        self.t = t

    def __str__(self):
        return 'Click at {}'.format(self.t)


class Clicks:
    def __init__(self, bd: bDur, silent: bool, clicks: List[Click],
                 start_time: cTime, end_time: cTime):

        self.bd = bd
        self.silent = silent
        self.clicks = clicks
        self.start_time = start_time
        self.end_time = end_time

    def __str__(self):
        if self.silent:
            s = 'rests'
        else:
            s = 'clicks'

        return '{} of {}, duration {:.2f}-{:.2f}, {} clicks'.format(
            self.bd, s, self.start_time.t, self.end_time.t, len(self.clicks))

    def audibleClickTimes(self):
        if self.silent:
            return []
        else:
            return self.clicks

    def dur_seconds(self):
        return self.end_time.t - self.start_time.t


class t2cState(timeState):
    def __init__(self):
        super().__init__()
        self.cbp = CBP(1)

    def __str__(self):
        return 'Tempo {}, TimeSignature {} ({}), rit {}, bar:beat {}:{}, time {:.3f}'.format(
            self.tempo, self.ts, self.cbp, self.rit, self.time.bar, self.time.beat, self.time.t)

