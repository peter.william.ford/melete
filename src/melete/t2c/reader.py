from melete.t2c.common import CBP, Click, Clicks, t2cState
import melete.core.mtime as mtime
from melete.core.mtime import TimeSignature, Tempo, cTime, bDur, FixedRit, Pause
from melete.core.event import EventHolder, EventType

from copy import deepcopy
import argparse
import sys

sep_char = ';'
verbosity = 0


def TimeSignature_parser(st, ln, els):
    ts = TimeSignature.from_str(els[0])
    if st.time.beat != 0:
        raise ValueError('Time Signature {} on line {} comes at beat {} in a bar'.format(
                            ts, ln, st.time.beat + 1))

    st.ts = ts
    e = EventHolder(st.time, ts, EventType.TS)
    if verbosity >= 1:
        print('From line {: >3}, {}'.format(ln, e))

    return e

def tempo_parser(st, ln, els):
    tempo = Tempo.from_list(els)

    st.tempo = tempo
    if st.rit:
        print('Warning, Tempo at line {} overrides ongoing rit {}'.format(ln, st.rit))

    st.rit = None
    e = EventHolder(st.time, tempo, EventType.tempo)
    if verbosity >= 1:
        print('From line {: >3}, {}'.format(ln, e))

    return e

def rit_parser(st, ln, els):
    if not (len(els) == 7 and els[2] == '->' and els[5] == 'over' and
                els[1].endswith('pm') and els[4].endswith('pm')):
        raise ValueError('Rit must be of the form 120 4pm -> 90 4pm over 1:1.5, not {}'.format(' '.join(els)))

    t1 = Tempo.from_list(els[:2])
    t2 = Tempo.from_list(els[3:5])
    dur = bDur.from_str(els[6], st.ts)
    rit = FixedRit(st.time, t1, t2, dur)

    st.rit = rit
    e = EventHolder(st.time, rit, EventType.rit)
    if verbosity >= 1:
        print('From line {: >3}, {}'.format(ln, e))

    return e

def click_parser(st, ln, type_str, els):
    if st.time is None or st.tempo is None or st.ts is None:
        raise ValueError('Must know tempo, time, and TimeSignature to process clicks')

    if len(els) != 1:
        raise ValueError('Cannot parse clicks at line {}: {}'.format(ln, els))

    clicks = []

    start = st.time

    total_bd = bDur.from_str(els[0], st.ts)
    first_frac = st.bd_to_next_beat()
    if total_bd < first_frac:
        # No clicks
        st.advance_by_bd(total_bd)
    else:
        st.advance_by_bd(first_frac)
        if verbosity >= 2:
            print('  Click: {}'.format(st.time))

        clicks.append(Click(st.time))
        accum_bd = first_frac
        click_bd_delta = bDur(1.0 / st.cbp.cbp, st.ts)
        while (accum_bd + click_bd_delta <= total_bd):
            st.advance_by_bd(click_bd_delta)
            if verbosity >= 2:
                print('  Click: {}'.format(st.time))

            clicks.append(Click(st.time))
            accum_bd = accum_bd + click_bd_delta

        # Now we should have < a click_bd_delta to go
        final_frac = total_bd - accum_bd
        if abs(final_frac.total_beats) > mtime._t_eps:
            if final_frac.total_beats < 0:
                raise ValueError('This has gone wrong... at line {}'.format(ln))

            st.advance_by_bd(final_frac)

    e = EventHolder(start, Clicks(total_bd, type_str == 'si',
                                  clicks, start, st.time), EventType.clicks)
    if verbosity >= 1:
        print('From line {: >3}, {}'.format(ln, e))

    return e

def cbp_parser(st, ln, els):
    try:
        cbp = int(els[0])
    except Exception:
        cbp = float(els[0])

    if cbp < 1:
        raise ValueError('clicks per beat must be >= 1, at line {} read {}'.format(
                            ln, cbp))

    st.cbp = CBP(cbp)
    e = EventHolder(st.time, st.cbp, EventType.cbp)
    if verbosity >= 1:
        print('From line {: >3}, {}'.format(ln, e))

    return e


def pause_parser(st, ln, els):
    dur = float(els[0])
    if len(els) != 1 or dur <= 0.0:
        raise ValueError('Cannot parse pause at line {}: {}'.format(ln, els))

    start = st.time
    end = deepcopy(start)
    end.t = end.t + dur
    st.time = end
    e = EventHolder(start, Pause(start, end), EventType.pause)
    if verbosity >= 1:
        print('From line {: >3}, {}'.format(ln, e))

    return e

def cmd_parser(s, st, ln):
    if s.count(sep_char) != 0 or len(s.splitlines()) != 1:
        raise ValueError('cmd contains a {} or a newline'.format(sep_char))

    els = s.split()
    if len(els) == 0:
        raise ValueError('No elements found in cmd {}'.format(s))

    c = els[0]
    if c == 'ts':
        return TimeSignature_parser(st, ln, els[1:])
    elif c == 'te':
        return tempo_parser(st, ln, els[1:])
    elif c == 'rit':
        return rit_parser(st, ln, els[1:])
    elif c == 'cl' or c == 'si':
        return click_parser(st, ln, c, els[1:])
    elif c == 'cbp':
        return cbp_parser(st, ln, els[1:])
    elif c == 'pa':
        return pause_parser(st, ln, els[1:])
    else:
        raise ValueError('Command {} not recognised in {}'.format(c, s))


def t2cReader(in_name, v):
    global verbosity
    verbosity = v
    with open(in_name, 'r', encoding='utf-8') as inf:
        content = [l.rstrip() for l in inf]

        state = t2cState()
        cmds = []
        for ln, l in enumerate(content):
            for s in l.split(sep_char):
                if s.startswith('%'):
                    continue

                try:
                    c = cmd_parser(s, state, ln + 1)
                except Exception:
                    print('Parsing failure at line {}, command {}'.format(ln + 1, s) + str(sys.exc_info()[0]))
                    raise

                cmds.append(c)

        return cmds, state


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=str, help='Input .t2c file')
    parser.add_argument('verbosity', type=int, help='Verbosity: 0=Warnings, 1=Verbose, 2=Debug')

    SETTINGS, unparsed = parser.parse_known_args()

    cmds, state = t2cReader(SETTINGS.infile, SETTINGS.verbosity)
    print('Final state: {}'.format(state))
