from melete.core.event import EventHolder, EventType
from melete.core.mtime import TimeSignature, Tempo, cTime, bTime, bDur, Rit, Pause
import melete.core.mtime as mtime
from melete.t2c.common import CBP, Click, Clicks, t2cState

import argparse

sep_char = ';'
verbosity = 0

def TS_writer(ts: TimeSignature):
    return 'ts {}/{}'.format(ts.num, ts.den.divisor)

def Tempo_writer(te: Tempo):
    return 'te {:.3f} {}pm'.format(te.bpm, te.unit.divisor)

def rit_writer(r: Rit):
    return 'rit {:.3f} {}pm -> {:.3f} {}pm over {}:{}'.format(
        r.t1.bpm, r.t1.unit.divisor, r.t2.bpm, r.t2.unit.divisor,
        r.bd.bars, r.bd.beats_proper
    )

def click_writer(c: Clicks):
    if c.silent:
        cmd = 'si'
    else:
        cmd = 'cl'

    return '{} {}:{}'.format(cmd, c.bd.bars, c.bd.beats_proper)

def cbp_writer(cbp: CBP):
    return 'cbp {}'.format(cbp.cbp)

def pause_writer(p: Pause):
    return 'pa {}'.format(p.dur())

def t2cWriter(cmds, out_name):
    with open(out_name, 'w', encoding='utf-8') as outf:
        glob_time = bTime(0, 0)
        outf.write('% {} t2c commands'.format(len(cmds)))
        last_advanced = True
        for c in cmds:
            this_advanced = c.type is EventType.pause or c.type is EventType.clicks or c.type is EventType.pause
            if last_advanced and this_advanced:
                outf.write('; ')
            elif not last_advanced and not this_advanced:
                outf.write('; ')
            elif last_advanced and not this_advanced:
                outf.write('\n    ')
            elif not last_advanced and this_advanced:
                outf.write('\n% Bar {}\n'.format(glob_time))

            if c.type is EventType.TS:
                outf.write(TS_writer(c.e))
            elif c.type is EventType.tempo:
                outf.write(Tempo_writer(c.e))
            elif c.type is EventType.rit:
                outf.write(rit_writer(c.e))
            elif c.type is EventType.clicks:
                outf.write(click_writer(c.e))
                glob_time = glob_time + c.e.bd
            elif c.type is EventType.cbp:
                outf.write(cbp_writer(c.e))
            elif c.type is EventType.pause:
                outf.write(pause_writer(c.e))
            else:
                raise ValueError('Cmd {} not recongised'.format(c))

            last_advanced = this_advanced
