from melete.core.note import qNote
from melete.core.pitch import NoteName, Pitch, Key, MinorKey
from melete.core.mtime import qTime, Frac, TimeSignature

from typing import List

def printNoteName(n: NoteName):
    s = n.NN.name.lower()
    if n.shift > 0:
        s += 's' * n.shift
    elif n.shift < 0:
        s += 'f' * (-1 * n.shift)
    return s


def printPitch(p: Pitch):
    if p is None:
        return 'r'

    s = printNoteName(p.name)
    oct_diff = p.octave - 3
    if oct_diff > 0:
        s += '\'' * oct_diff
    elif oct_diff < 0:
        s += ',' * (-1 * oct_diff)
    return s


def printNote(note: qNote, ts: TimeSignature):
    fragments = note.get_duration_fragments(ts)
    pitch = printPitch(note.pitch)
    note_strs = []
    denom = note.qstart.tf.beat_type
    for i in range(len(fragments)):
        f = fragments[i]
        b, _, d = f.qduration_frac()
        if b == 1:
            # n should be 0
            note_strs.append(pitch + str(denom.divisor))
        elif d in [1, 2, 4, 8, 16, 32, 64]:
            pairs = f.split_binary()
            for [pn, pd] in pairs:
                for j in range(pn):
                    note_strs.append(pitch + str(denom.divisor * pd))
        else:
            raise ValueError('Tuplets ({}) not yet handled'.format(d))

    if note.pitch is None:
        return ' '.join(note_strs)
    else:
        return '~ '.join(note_strs)


def printNotes(notes: List[qNote], ts: TimeSignature):
    # TODO! notes.make_non_overlapping()
    #TODO: Handle rests between notes!!
    first_note = notes[0]
    first_rest = qNote(None, qTime(0, Frac(0, 0, 1), 0.0), first_note.start)
    out = printNote(first_rest) + '\n'

    last_bar = first_note.start.bar
    for n in notes:
        if n.start.bar != last_bar:
            out += '\n\n % Bar {}\n    '.format(n.start.bar + 1)
            last_bar = n.start.bar

        out += printNote(n) + ' '

    return out


def printKey(k: Key):
    if isinstance(k, MinorKey):
        return printNoteName(k.noteName) + ' \minor'
    else:
        return printNoteName(k.noteName) + ' \major'


def write_single_line_file(notes: List[qNote], fname: str, key: Key, ts: TimeSignature):
    with open(fname, 'w', encoding='utf-8') as fo, open('preamble.ly', 'r') as fi:
        fo.write(fi.read())
        fo.write('\n\n\\header {\n')
        fo.write('  title = "{}"\n'.format(fname.strip('"')))
        fo.write('}\n\n')

        fo.write('melody = {\n')
        fo.write('  \\clef treble\n')
        fo.write('  \\key {}\n'.format(printKey(key)))
        fo.write('  \\time {}\n'.format(str(ts)))
        fo.write(printNotes(notes))
        fo.write('\n}\n\n')

        fo.write('\\score {\n')
        fo.write('  \\new Staff { \\new Voice { \\melody } }\n')
        fo.write('  \\layout { }\n')
        fo.write('}\n')
