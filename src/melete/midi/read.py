import mido
import math
from mido import MetaMessage, Message
from melete.midi.common import midiTrack, midiGlobal, midiFile, midiProgChange, midiNote
from melete.core.mtime import Tempo, TimeSignature, SemibreveFraction, TimeFrame, fTime, fDur
from melete.core.pitch import Pitch, key_from_str
from melete.core.note import Note

def parse_file(mf: mido.MidiFile):
    N_tr = len(mf.tracks)
    TSes = []
    tempos = []
    strings = []
    inst_names = [None] * N_tr
    track_names = [None] * N_tr

    tpb = mf.ticks_per_beat

    for i, t in enumerate(mf.tracks):
        for m in t:
            if m.is_meta:
                if m.type == 'text' or m.type == 'copyright':
                    strings.append(m.text)
                elif m.type == 'track_name':
                    track_names[i] = m.name
                elif m.type == 'instrument_name':
                    inst_names[i] = m.name
                elif m.type == 'set_tempo':
                    # TODO: Track time for these!
                    tempos.append((fTime(0), Tempo(mido.tempo2bpm(m.tempo), SemibreveFraction(4))))
                elif m.type == 'time_signature':
                    TSes.append(
                        (0, TimeSignature(m.numerator, SemibreveFraction(m.denominator))))
                #Ignoring various other meta types, in particular key_signature

    #TODO: Deal with changes of TS...
    if len(TSes) != 1:
        raise ValueError('Can only handle MIDI files with exactly 1 time-signature, this has {}'.format(
            len(TSes)))

    TS = TSes[0][1]

    durations = [fDur(b1[0].d - b0[0].d) for b0, b1 in zip(tempos[:-1], tempos[1:])]
    durations.append(1000)  # TODO: get length of file

    TFs = []
    for b, d in zip(tempos, durations):
        time = b[0]
        bpm = b[1]
        N = math.ceil(d * bpm) + 1
        #TODO! TFs.append((time, TimeFrame.from_bpm_and_ts(bpm, TS, N, time)))

    globs = midiGlobal(TSes, tempos, TFs)

    # TODO!
    key = key_from_str('Am')

    #TODO: Just assume everything in a track is on one channel for now.
    prog_changes = []
    tracks = []
    for i, tr in enumerate(mf.tracks):
        last_notes = [None] * 127
        mnotes = []
        tick = 0
        time = 0.0
        for m in tr:
            tick_delta = m.time
            #TODO This ignores tempo changes during a time-delta, but they seem stupid anyway.
            ticks_per_s = globs.tempo_at_time(time) * tpb / 60.0
            tick += tick_delta
            time += tick_delta / ticks_per_s
            if not m.is_meta:
                if m.type == 'program_change':
                    prog_changes.append(midiProgChange(
                        time, m.channel, m.program))
                elif m.type == 'note_on' and m.velocity > 0:
                    if last_notes[m.note]:
                        raise ValueError('A note {} has not been finished, and we''re trying to start another'.format(
                            last_notes[m.note]))

                    last_notes[m.note] = midiNote(
                        time, m.note, m.velocity, m.channel)
                elif (m.type == 'note_on' and m.velocity == 0) or m.type == 'note_off':
                    if last_notes[m.note] is None:
                        raise ValueError(
                            'Trying to end a note at pitch {} without a start'.format(m.note))

                    # TODO: Add volume to mnotes.Note
                    L = last_notes[m.note]
                    s = L.start
                    p = Pitch(L.pitch, key.bestNoteName(L.pitch))
                    duration = time - s
                    mnotes.append(Note(p, s, duration))
                    last_notes[m.note] = None
                else:
                    pass  # Messages we ignore

        track = midiTrack(track_names[i], inst_names[i], mnotes, [])
        tracks.append(track)

    return midiFile(mf.filename, globs, tracks)
