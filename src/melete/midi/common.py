import mido
from mido import MetaMessage, Message
from typing import List, Tuple

from melete.core.pitch import NoteName
from melete.core.mtime import TimeSignature, TimeFrame, Tempo, fTime
from melete.core.note import Note


class midiTrack:
    def __init__(self, name: str, instr_name: str, notes: List[Note], metas: List[MetaMessage]):
        self.name = name
        self.instr_name = instr_name
        self.notes = notes
        self.metas = metas

    def __str__(self):
        return 'MIDI track {} for instrument {} with {} notes and {} meta messages'.format(
            self.name, self.instr_name, len(self.notes), len(self.metas))


class midiGlobal:
    def __init__(self, TSes: List[Tuple[float, TimeSignature]], tempos: List[Tuple[fTime, Tempo]],
                 TFs=List[Tuple[float, TimeFrame]]):
        self.TSes = TSes
        self.tempos = tempos
        self.TFs = TFs

    def TF_at_time(self, time: float):
        last_TF = None
        for t, tf in self.TFs:
            if t <= time:
                last_TF = tf

        return last_TF
        #TODO: Test!!

    def tempo_at_time(self, time: float):
        last_tempo = None
        for t, tempo in self.tempos:
            if t <= time:
                last_tempo = tempo
        return last_tempo


class midiFile:
    def __init__(self, name: str, globs: midiGlobal, tracks: List[midiTrack]):
        self.name = name
        self.globals = globs
        self.tracks = tracks


class midiProgChange:
    def __init__(self, time, channel, program):
        self.time = time
        self.channel = channel
        self.program = program


class midiNote:
    def __init__(self, start: float, pitch: int, velocity: int, channel: int):
        self.start = start
        self.pitch = pitch
        self.velocity = velocity
        self.channel = channel

    def note_name(self):
        #TODO: Get key
        return NoteName.from_semitone(self.pitch, True)

    def __str__(self):
        return 'Note {} started at {}, v = {}, channel = {}'.format(
            self.note_name(), self.start, self.velocity, self.channel)
