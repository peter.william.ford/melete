from enum import Enum
import functools

class Unic:
    sh = '♯'
    fl = '♭'
    nat = '♮'


class NNN(Enum):
    C = 0
    D = 2
    E = 4
    F = 5
    G = 7
    A = 9
    B = 11

    @classmethod
    def values(cls):
        return [e.value for e in cls]

    @classmethod
    def names(cls):
        return [e.name for e in cls]

    @classmethod
    def from_degree(cls, d):
        d = d % 7
        return cls(NNN.values()[d])

    def degree(self):
        return NNN.values().index(self.value)

    def next_up(self):
        return NNN.from_degree(self.degree() + 1)

    def next_down(self):
        return NNN.from_degree(self.degree() - 1)


class NoteName:
    def __init__(self, natNote: NNN, shift: int = 0):
        if not isinstance(natNote, NNN):
            raise TypeError("natNote must be an NNN")

        self.NN = natNote
        self.shift = shift

    def __eq__(self, other):
        return self.NN == other.NN and self.shift == other.shift

    @classmethod
    def from_semitone(cls, st, lower):
        st = st % 12
        l = NNN.values()
        if lower:
            base = max(i for i in l if i <= st)
            diff = st - base
            return cls(NNN(base), diff)
        else:
            base = min(i for i in l if i >= st)
            diff = st - base
            return cls(NNN(base), diff)

    @classmethod
    def from_semitone_and_NNN(cls, st: int, NN: NNN):
        diff = (st - NN.value) % 12
        if diff > 6:
            diff -= 12
        return cls(NN, diff)

    @classmethod
    def from_str(cls, s):
        if s[0].upper() not in NNN.names():
            raise TypeError('Str {} must start with a NNN'.format(s))

        if len(s) > 1 and s[1] == 'f':
            return cls(NNN[s[0].upper()], -1 * (len(s) - 1))

        if len(s) > 1 and s[1] == '#':
            return cls(NNN[s[0].upper()], len(s) - 1)

        return cls(NNN[s[0].upper()], 0)

    @property
    def semitone(self):
        return (self.NN.value + self.shift) % 12

    def normalise(self):
        while self.shift < -2:
            next_n = NoteName.from_semitone_and_NNN(
                self.semitone, self.NN.next_down())
            self.NN = next_n.NN
            self.shift = next_n.shift

        while self.shift > 2:
            next_n = NoteName.from_semitone_and_NNN(
                self.semitone, self.NN.next_up())
            self.NN = next_n.NN
            self.shift = next_n.shift

    def __str__(self):
        suffix = ''
        if self.shift > 0:
            suffix = Unic.sh * self.shift
        elif self.shift < 0:
            suffix = Unic.fl * (-1 * self.shift)
        return self.NN.name + suffix


class Key:
    def __init__(self, noteName: NoteName):
        if not isinstance(noteName, NoteName):
            raise TypeError("noteName must be a NoteName")
        self.noteName = noteName

    def __str__(self):
        s = 'Key {0} ({1} sharps, {2} flats), key signature = {3}'.format(
            self.name, self.nSharps(), self.nFlats(),
            ', '.join([str(n) for n in self.keySignature()]))

        e = self.melodicNotes()
        if len(e) > 0:
            s += ', other notes = {0}'.format(', '.join([str(n) for n in e]))

        return s

    @functools.singledispatchmethod
    def __eq__(self, other):
        raise NotImplementedError(other)

    @property
    def name(self):
        return '{0} major'.format(self.noteName)

    def bestNoteName(self, st: int):
        e = self.melodicNotes()
        inKey = self.keySignature()
        if len(e) > 0:
            inKey += e

        for n in inKey:
            if (n.semitone - st) % 12 == 0:
                return n

        root = inKey[0]
        # TODO: Just a random hardcoded lookup, for now.
        diff = (st - root.semitone) % 12
        if diff == 1:
            n = inKey[1]
            n.shift -= 1
            return n
        if diff == 3:
            n = inKey[2]
            n.shift -= 1
            return n
        if diff == 4:
            n = inKey[2]
            n.shift += 1
            return n
        if diff == 6:
            #tritone
            l = inKey[3]
            l.shift += 1
            u = inKey[4]
            u.shift -= 1
            if abs(l.shift) <= abs(u.shift):
                return l
            else:
                return u
        if diff == 8:
            n = inKey[5]
            n.shift -= 1
            return n
        if diff == 10:
            n = inKey[6]
            if n.semitone != st:
                n.shift -= 1
            return n

    def keySignature(self):
        keyDegree = NNN.names().index(self.noteName.NN.name)
        notes = []
        scaleDegree = 0
        for _, member in NNN.__members__.items():
            semitonesUp = member.value - NNN(0).value
            degreeNNN = NNN.from_degree(keyDegree + scaleDegree)
            st = self.noteName.semitone + semitonesUp
            notes.append(NoteName.from_semitone_and_NNN(st, degreeNNN))
            scaleDegree += 1

        return notes

    def melodicNotes(self):
        return []

    def nSharps(self):
        total = 0
        for n in self.keySignature():
            if n.shift > 0:
                total += n.shift
        return total

    def nFlats(self):
        total = 0
        for n in self.keySignature():
            if n.shift < 0:
                total -= n.shift
        return total

class MinorKey(Key):
    def __init__(self, noteName: NoteName):
        super().__init__(noteName)

    @property
    def name(self):
        return '{0} minor'.format(self.noteName)

    def melodicNotes(self):
        scale = self.keySignature()
        extras = []
        note = scale[-2]
        note.shift = note.shift + 1
        extras.append(note)
        note = scale[-1]
        note.shift = note.shift + 1
        extras.append(note)
        return extras

    def keySignature(self):
        keyDegree = NNN.names().index(self.noteName.NN.name)
        notes = []
        scaleDegree = 0
        l = list(NNN.__members__.items())
        l = l[-2:] + l[:-2]
        for _, member in l:
            semitonesUp = (member.value - NNN['A'].value) % 12
            degreeNNN = NNN.from_degree(keyDegree + scaleDegree)
            st = self.noteName.semitone + semitonesUp
            notes.append(NoteName.from_semitone_and_NNN(st, degreeNNN))
            scaleDegree += 1

        return notes

@Key.__eq__.register
def _(self, other: Key):
    return self.noteName == other.noteName and isinstance(other, MinorKey) == isinstance(self, MinorKey)



def key_from_str(s):
    if s[-1] == 'm':
        return MinorKey(NoteName.from_str(s[:-1]))
    else:
        return Key(NoteName.from_str(s))


class Pitch:
    def __init__(self, midinote: int, nn: NoteName):
        if not isinstance(nn, NoteName):
            raise TypeError('nn must be a NoteName')

        self.mn = int(midinote)
        self.name = nn
        if (nn.semitone - self.mn) % 12 != 0:
            raise ValueError('midinote ({}, {}) and NoteName ({}) inconsistent'.format(
                midinote, NoteName.from_semitone(midinote, True), nn))

    @property
    def octave(self):
        #TODO Check
        return (self.mn - self.name.semitone) // 12 - 1

    def __str__(self):
        return str(self.name) + str(self.octave)

    def __eq__(self, other):
        return self.mn == other.mn and self.name == other.name
