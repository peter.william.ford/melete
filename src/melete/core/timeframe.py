import melete.core.mtime as mtime
from melete.core.mtime import TimeSignature, Tempo, fDur, fTime, cTime, bTime, qTime, Frac
from melete.core.mtime import bDur, Rit, Pause, SemibreveFraction, FixedRit, qStartEnd
from melete.core.event import EventHolder, EventType
from melete.core.note import qNote

from copy import deepcopy
from math import ceil, floor
from typing import List

verbosity = 0

class timeState:
    def __init__(self):
        self.tempo = Tempo(120, SemibreveFraction(4))
        self.ts = TimeSignature(4, SemibreveFraction(4))
        self.rit = None
        self.time = cTime(0, 0, 0)

    def __str__(self):
        return 'Tempo {}, TimeSignature {}, rit {}, bar:beat {}:{}, time {:.3f}'.format(
            self.tempo, self.ts, self.rit, self.time.bar, self.time.beat, self.time.t)

    def bd_to_next_beat(self):
        '''Returning 1 at an integer'''
        if self.time.is_integer:
            frac = 1
        else:
            frac = ceil(self.time.beat) - self.time.beat

        return bDur(frac, self.ts)

    def advance_by_bd(self, bd: bDur):
        next_bb = self.time.get_bTime() + bd

        if self.rit is not None:
            rit_end = self.rit.end()

            if self.time.get_bTime() >= rit_end.get_bTime():
                raise ValueError('Rit error at state {}'.format(self))

            if rit_end.get_bTime() < next_bb:
                # The rit ends between now and the next beat
                # Advance to end of rit first
                self.time = rit_end
                self.tempo = self.rit.t2
                self.rit = None
                self.advance_by_bd(next_bb.sub_using_TS(self.time, self.ts))
                return

            else:
                self.time = self.rit.time_at_BBtime(next_bb)
                if self.time.get_bTime() == rit_end.get_bTime():
                    self.tempo = self.rit.t2
                    self.rit = None
        else:
            self.time = self.time.plus_dur_at_tempo(bd, self.tempo)


class TimeFrame:
    def __init__(self, beats: List[qTime], beat_type: SemibreveFraction):
        self.beats = beats
        self.beat_type = beat_type

    @classmethod
    def from_tempo_and_ts(cls, tempo: Tempo, ts: TimeSignature, N: int, offset=0.0):
        N = int(N)
        step = tempo.beat_time_delta()
        beats = []
        for i in range(N):
            bar = i // ts.num
            beat = i - (bar * ts.num)
            time = offset + step * i
            beats.append(qTime(bar, Frac(beat, 0, 1), time))

        return cls(beats, ts.den)

    def __getitem__(self, key):
        return self.beats[key]

    def avg_duration(self):
        if len(self.beats) > 1:
            return (self.beats[-1].t - self.beats[0].t) / (len(self.beats) - 1)
        else:
            return 1.0

    def ts_of_bar(self, bar_idx: int):
        bar_beats = [b for b in self.beats if b.bar == bar_idx]
        if len(bar_beats) == 0:
            raise ValueError('Bar {} not found'.format(bar_idx))

        return TimeSignature(bar_beats[-1].qbeat.I + 1, self.beat_type)

    def start(self):
        return fTime(self.beats[0].t)

    def end(self):
        return fTime(self.beats[-1].t)

    def duration(self, idx):
        if idx < 0 or idx + 1 >= len(self.beats):
            return None

        return self.beats[idx + 1].t - self.beats[idx].t

    def beat_before(self, time: float):
        b, i = next(((b, i) for i, b in enumerate(reversed(self.beats))
                     if b.get_fTime() <= fTime(time)), (None, None))
        if i is not None:
            i = len(self.beats) - 1 - i
        return deepcopy(b), i

    def beat_after(self, time: float):
        b, i = next(((b, i) for i, b in enumerate(self.beats)
                     if b.get_fTime() > fTime(time)), (None, None))
        return deepcopy(b), i

    def beat_closest(self, time: float):
        diffs = [abs(b.t - time) for b in self.beats]
        idx = diffs.index(min(diffs))
        return deepcopy(self.beats[idx]), idx

    def bar_starts(self):
        bars = []
        idx = None
        for b in self.beats:
            if b.bar != idx:
                if idx is not None and abs(b.beat - 0.0) > mtime._t_eps:
                    raise ValueError('First beat of bar {} is missing, found {}'.format(idx+1, b))

                bars.append(b)
                idx = b.bar

        return bars

    def bars(self):
        bars = []
        idx = None
        this_bar = None
        for b in self.beats:
            if b.bar != idx:
                if idx is not None and abs(b.beat - 0.0) > mtime._b_eps:
                    raise ValueError(
                        'First beat of bar {} is missing, found {}'.format(idx+1, b))
                if this_bar is not None:
                    bars.append(this_bar)

                this_bar = [b]
                idx = b.bar
            else:
                this_bar.append(b)

        if this_bar is not None:
            bars.append(this_bar)

        return bars

    def get_cTime(self, t: fTime):
        if t < self.start():
            raise ValueError(
                'Can\'t get cTime for {}, before start {}'.format(t, self.beats[0]))
        if t > self.end() + fDur(self.avg_duration() - mtime._t_eps):
            raise ValueError(
                'Can\'t get cTime for {}, after end {}'.format(t, self.beats[-1]))

        beat, idx = self.beat_before(t.t)
        beat = beat.get_cTime()
        if idx == len(self.beats) - 1:
            duration = self.avg_duration()
        else:
            duration = self.beats[idx + 1].t - beat.t

        beat_prop = (t.t - beat.t) / duration
        if beat_prop < -1.5 * mtime._t_eps or beat_prop > 1 + mtime._t_eps:
            raise ValueError('Invalid beats? beat_prop={}, idx={}, beat={}, next_beat={}'.format(
                beat_prop, idx, beat, self.beats[idx + 1]))
        beat.beat += beat_prop
        beat.t = t.t
        return beat, idx

    def get_best_qTime(self, t: fTime, Dset: List[int] = [1, 2, 3, 4, 8]):
        if t < self.start():
            raise ValueError(
                'Can\'t get qTime for {}, before start {}'.format(t, self.beats[0]))
        if t > self.end() + fDur(self.avg_duration() - 1/max(Dset)):
            raise ValueError(
                'Can\'t get qTime for {}, after end {}'.format(t, self.beats[-1]))

        c, idx = self.get_cTime(t)
        if c.beat < 0:
            if abs(c.beat) < mtime._t_eps:
                c.beat = 0
            else:
                raise ValueError('Negative beat?? {}'.format(c.beat))

        ts = self.ts_of_bar(c.bar)
        qt = qTime.from_cTime(c, ts, Dset)
        if qt.qbeat.I != floor(c.beat):
            assert (qt.qbeat.I - (floor(c.beat) + 1)) % ts.num == 0 and qt.qbeat.N == 0 and qt.qbeat.D == 1

        return qt, idx


    def get_qSE(self, start: fTime, end: fTime, Dset: List[int] = [1, 2, 3, 4, 8]):
        if self.end <= self.start:
            raise ValueError(
                'End {} must be after Start {}'.format(self.end, self.start))

        q1 = self.get_best_qTime(start, Dset)
        q2 = self.get_best_qTime(end, Dset)
        if q1.bar == q2.bar and q1.qbeat.I == q2.qbeat.I:
            print('Should calculate this separately')

        return qStartEnd(q1, q2)

    def __repr__(self):
        return 'TimeFrame {}-{}, {} beats upto {}'.format(
            self.start(), self.end(),
            len(self.beats), self.beats[-1])

    def __str__(self):
        return self.__repr__()

def TS_handler(st, ts: TimeSignature):
    if ts.den != st.ts.den:
        raise ValueError('TimeFrame only handles fixed denominator TSes, trying to change from {} to {}'.format(
            st.ts, ts
        ))
    st.ts = ts

def tempo_handler(st, tempo: Tempo):
    st.tempo = tempo
    if st.rit:
        print('Warning, Tempo {} overrides ongoing rit {}'.format(tempo, st.rit))

    st.rit = None

def rit_handler(st, rit: Rit):
    if rit.bd.ts != st.ts:
        raise ValueError('TimeSignature in Rit {} doesn\'t match current TS {}'.format(rit.bd.ts, st.ts))

    st.rit = FixedRit.from_time_and_rit(st.time, rit)

def pause_handler(st, pause: Pause):
    if pause.start != st.time:
        raise ValueError('Pause time specified incorrectly: {}, we are at {}'.format(pause.start, st.time))

    st.time = pause.end

def add_beats(st, tf: TimeFrame, target_bd: bDur):
    if st.time is None or st.tempo is None or st.ts is None:
        raise ValueError(
            'Must know tempo, time, and TimeSignature to add beats')

    first_frac = st.bd_to_next_beat()
    if target_bd < first_frac:
        # No beats
        st.advance_by_bd(target_bd)
    else:
        st.advance_by_bd(first_frac)
        if verbosity >= 2:
            print('  Beat: {}'.format(st.time))

        tf.beats.append(st.time)
        accum_bd = first_frac
        beat_bd_delta = bDur(1.0, st.ts)
        while (accum_bd + beat_bd_delta <= target_bd):
            st.advance_by_bd(beat_bd_delta)
            if verbosity >= 2:
                print('  Beat: {}'.format(st.time))

            tf.beats.append(st.time)
            accum_bd = accum_bd + beat_bd_delta

        # Now we should have < a click_bd_delta to go
        final_frac = target_bd - accum_bd
        if abs(final_frac.total_beats) > mtime._t_eps:
            if final_frac.total_beats < 0:
                raise ValueError(
                    'This has gone wrong... at time {}'.format(st.time))

            st.advance_by_bd(final_frac)


class NoteOverlapError(ValueError):
    def __init__(self, q1: qNote, q2: qNote, message: str):
        self.q1 = q1
        self.q2 = q2
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return '{}: {} and {} overlap'.format(self.message, self.q1, self.q2)

class StructuredTimeFrame(TimeFrame):
    '''Construct timeframe from Tempos, TimeSignatures, Rits, and Pauses'''

    def __init__(self, tf: TimeFrame, events: List[EventHolder]):
        events = sorted(events, key=lambda eh: eh.t)
        self.events = events
        super().__init__(tf.beats, tf.beat_type)
        self.TSes = []
        self.Tempos = []
        self.Rits = []
        self.Pauses = []

        for eh in events:
            if eh.type is EventType.TS:
                self.TSes.append(eh)
            elif eh.type is EventType.tempo:
                self.TSes.append(eh)
            elif eh.type is EventType.rit:
                self.TSes.append(eh)
            elif eh.type is EventType.pause:
                self.TSes.append(eh)

    @classmethod
    def from_events(cls, init_ts: TimeSignature, events: List[EventHolder], end: bTime):
        tf = TimeFrame([cTime(0, 0, 0)], init_ts.den)
        events = sorted(events, key=lambda eh: eh.t)

        state = timeState()
        for eh in events:
            if eh.t < state.time:
                raise ValueError('Going backwards in time from {} to {}'.format(state.time, eh.t))

            if eh.t > state.time:
                add_beats(state, tf, eh.t.sub_using_TS(state.time, state.ts))

            if eh.type is EventType.TS:
                TS_handler(state, eh.e)
            elif eh.type is EventType.tempo:
                tempo_handler(state, eh.e)
            elif eh.type is EventType.rit:
                rit_handler(state, eh.e)
            elif eh.type is EventType.pause:
                pause_handler(state, eh.e)

        if end < state.time:
            raise ValueError('Going backwards in time from {} to {}'.format(state.time, end))
        if end > state.time:
            add_beats(state, tf, end.sub_using_TS(state.time, state.ts))

        return cls(tf, events)


    def __repr__(self):
        return 'StructuredTimeFrame, {} events and {} beats upto {}'.format(
            len(self.events), len(self.beats), self.beats[-1])

    def __str__(self):
        s = self.__repr__()
        for eh in self.events:
            s += '\n    {}'.format(eh)

        return s

    def get_gap(self, q1: qNote, q2: qNote):
        if q1.end > q2.start:
            raise NoteOverlapError(q1, q2, "Negative gap between notes")

        if q1.end < q2.start:
            return qStartEnd(q1.end, q2.start)

        return None


