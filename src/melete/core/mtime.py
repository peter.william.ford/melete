from __future__ import annotations

from math import ceil, floor, log, gcd, inf
from typing import List
import functools
from copy import deepcopy

from melete.core.event import EventHolder, EventType


_t_eps = 0.0001
_b_eps = 0.0001

class SemibreveFraction:
    def __init__(self, divisor: int):
        divisor = int(divisor)
        if divisor > 0 and divisor & (divisor - 1) == 0:
            self.divisor = divisor
        else:
            raise ValueError(
                'divisor {} must be a power of 2 for now'.format(divisor))

    @staticmethod
    def name_list():
        return {1: 'semibreve', 2: 'minim', 4: 'crotchet', 8: 'quaver', 16: 'semiquaver',
                32: 'demisemiquaver', 64: 'hemidemisemiquaver'}

    def name(self):
        l = self.name_list()
        if self.divisor in l:
            return l[self.divisor]

        s = ''
        d = self.divisor
        while d > 64:
            s += 'hemi'
            d /= 2

        return s + l[d]

    def __eq__(self, y):
        return self.divisor == y.divisor

    def __str__(self):
        return self.name()


class TimeSignature:
    def __init__(self, numerator: int, denominator: SemibreveFraction):
        if not isinstance(denominator, SemibreveFraction):
            raise TypeError('Denominator {} must be a SemibreveFraction'.format(denominator))
        self.num = int(numerator)
        self.den = denominator

    def __str__(self):
        return '{}/{}'.format(self.num, self.den.divisor)

    @classmethod
    def from_str(cls, s):
        l = s.split('/')
        if len(l) != 2:
            raise ValueError('{} cannot be split as a/b'.format(s))

        return cls(int(l[0]), SemibreveFraction(int(l[1])))

    def __eq__(self, y):
        return self.num == y.num and self.den == y.den

    def primaries(self):
        return [0]

    def secondaries(self):
        if self.num % 3 == 0:
            return [3 * i for i in list(range(1, self.num // 3))]
        elif self.num == 4:
            return [2]
        else:
            return []

class Tempo:
    def __init__(self, bpm: float, unit: SemibreveFraction):
        if not isinstance(unit, SemibreveFraction):
            raise TypeError('Unit {} must be a SemibreveFraction'.format(unit))

        if isinstance(bpm, int):
            self.bpm = bpm
        else:
            self.bpm = float(bpm)

        self.unit = unit

    def __str__(self):
        if isinstance(self.bpm, int):
            return '{} {} beats per minute'.format(self.bpm, self.unit.name())
        else:
            return '{:.3f} {} beats per minute'.format(self.bpm, self.unit.name())

    def __eq__(self, other):
        return abs(self.bpm - other.bpm) < _b_eps and self.unit == other.unit

    def equiv(self, other):
        if not isinstance(other, Tempo):
            raise TypeError('other must be a Tempo, not {}'.format(other))

        return self.bpm * other.unit.divisor == other.bpm * self.unit.divisor

    def bps(self):
        return self.bpm / 60.0

    def beat_time_delta(self):
        return 1.0 / self.bps()

    def semibreve_time_delta(self):
        return self.beat_time_delta() * self.unit.divisor

    @classmethod
    def from_list(cls, l):
        if len(l) != 2:
            raise ValueError('{} cannot be split as a b'.format(l))

        if not isinstance(l[1], str):
            raise TypeError('{} must be a string'.format(l[1]))

        try:
            bpm = int(l[0])
        except:
            bpm = float(l[0])

        if l[1][-2:] != 'pm':
            raise ValueError('{} must be of the form 120 4pm'.format(l))

        return cls(bpm, SemibreveFraction(int(l[1][:-2])))

    @classmethod
    def from_str(cls, s):
        return cls.from_list(s.split())


class fDur:
    def __init__(self, d: float):
        self.d = float(d)

    def __str__(self):
        return '{:.3f} s'.format(self.d)

    def __repr__(self):
        return 'fDur: ' + str(self)

    def __eq__(self, other):
        return self.d == other.d

class bDur:
    def __init__(self, beats: float, ts: TimeSignature):
        if not isinstance(ts, TimeSignature):
            raise TypeError('ts is wrong type {}'.format(ts))

        self.ts = ts
        self.total_beats = float(beats)

    @property
    def is_integer(self):
        return abs(self.total_beats - round(self.total_beats)) < _b_eps

    @property
    def bars(self):
        if self.is_integer:
            return round(self.total_beats) // self.ts.num
        else:
            return floor(self.total_beats / self.ts.num)

    @property
    def beats_proper(self):
        return self.total_beats - self.ts.num * self.bars

    @classmethod
    def from_str(cls, s, ts: TimeSignature):
        l = s.split(':')
        if len(l) != 2:
            raise ValueError('{} must be a:b(.c)'.format(s))

        return cls.from_bars_and_beats(int(l[0]), float(l[1]), ts)

    @classmethod
    def from_bars_and_beats(cls, bars: int, beats: float, ts: TimeSignature):
        if beats >= ts.num or bars < 0 or beats < 0:
            raise ValueError('Improper bDur: {}:{}, {}'.format(bars, beats, ts))

        tot_beats = float(int(bars) * ts.num + float(beats))
        return cls(tot_beats, ts)

    def __add__(self, y):
        if not isinstance(y, bDur) or self.ts != y.ts:
            return NotImplemented

        return bDur(self.total_beats + y.total_beats, self.ts)

    def __sub__(self, y):
        b = self.total_beats - y.total_beats
        if self.ts != y.ts or b < 0:
            return NotImplemented

        return bDur(b, self.ts)

    def __eq__(self, y):
        return abs(self.total_beats - y.total_beats) < _b_eps

    def __le__(self, y):
        if self.ts != y.ts or not isinstance(y, bDur):
            return NotImplemented

        return self.total_beats < y.total_beats + _b_eps

    def __ge__(self, y):
        if self.ts != y.ts:
            return NotImplemented

        return self.total_beats > y.total_beats - _b_eps

    def __lt__(self, y):
        return not self >= y

    def __gt__(self, y):
        return not self <= y

    def semibreves_dur(self):
        return self.total_beats / self.ts.den.divisor

    def time_dur(self, tempo: Tempo):
        beat_time = tempo.semibreve_time_delta() / self.ts.den.divisor
        return self.total_beats * beat_time

    def __str__(self):
        b = self.total_beats - self.ts.num * self.bars
        if self.is_integer:
            beats_str = '{} beats'.format(round(b))
        else:
            beats_str = '{:.3f} beats'.format(b)

        if self.bars == 0:
            return beats_str
        else:
            return '{} bars of {}, {}'.format(
                self.bars, str(self.ts), beats_str)

class cDur(bDur):
    def __init__(self, beats: float, secs: float, ts: TimeSignature):
        super().__init__(beats, ts)
        self.secs = float(secs)

    @classmethod
    def from_bdur_and_fdur(cls, bd: bDur, fd: fDur):
        return cls(bd.total_beats, fd.d, bd.ts)

    @classmethod
    def from_bdur_and_tempo(cls, bd: bDur, t: Tempo):
        return cls.from_bdur_and_fdur(bd, fDur(t.beat_time_delta() * bd.total_beats))

    def __str__(self):
        return '{:.3f} s, {}'.format(self.secs, super().__str__())

    def __eq__(self, y):
        return abs(self.secs - y.secs) < _t_eps and super().__eq__(y)

    def get_bDur(self):
        return bDur(self.total_beats, self.ts)


class fTime:
    def __init__(self, t: float):
        self.t = float(t)

    def __str__(self):
        return 'fTime {:.3f}'.format(self.t)

    def __eq__(self, other):
        return self.t == other.t

    def __le__(self, other):
        return self.t < other.t + _t_eps

    def __ge__(self, other):
        return self.t > other.t - _t_eps

    def __lt__(self, y):
        return not self >= y

    def __gt__(self, y):
        return not self <= y

    def __add__(self, y):
        if not isinstance(y, fDur):
            return NotImplemented

        return fTime(self.t + y.d)

    def __sub__(self, y):
        if not isinstance(y, fDur):
            return NotImplemented

        return fTime(self.t - y.d)

# TODO: Can't check for 0:3.9999 vs 1:0.0 cases without knowing the TS of the previous bar.
# Should bTime have a "current TS"?
class bTime:
    def __init__(self, bar: int, beat: float):
        self.bar = int(bar)
        self.beat = float(beat)

    def __str__(self):
        if self.is_integer:
            return '{}:{}'.format(self.bar, round(self.beat))
        else:
            return '{}:{:.3f}'.format(self.bar, self.beat)

    def __repr__(self):
        return 'bTime ' + str(self)

    @property
    def is_integer(self):
        return abs(self.beat - round(self.beat)) < _b_eps

    def __eq__(self, y):
        return self.bar == y.bar and abs(self.beat - y.beat) < _b_eps

    def __le__(self, y):
        if self.bar == y.bar:
            return self.beat < y.beat + _b_eps
        else:
            return self.bar < y.bar

    def __ge__(self, y):
        if self.bar == y.bar:
            return self.beat + _b_eps > y.beat
        else:
            return self.bar > y.bar

    def __lt__(self, y):
        return not self >= y

    def __gt__(self, y):
        return not self <= y

    @functools.singledispatchmethod
    def __add__(self, other):
        raise NotImplementedError(other)

    @__add__.register
    def _(self, bd: bDur):
        beat = self.beat + bd.beats_proper
        bar = self.bar + bd.bars
        if beat > bd.ts.num - _b_eps:
            beat -= bd.ts.num
            bar += 1

        return bTime(bar, beat)

    def sub_using_TS(self, t2: 'bTime', ts: TimeSignature):
        if t2 > self:
            raise ValueError('time1 {} < time2 {}'.format(self, t2))

        bars = self.bar - t2.bar
        beats = ts.num * bars + (self.beat - t2.beat)
        return bDur(beats, ts)

class cTime(bTime):
    def __init__(self, bar: int, beat: float, time: float):
        super().__init__(bar, beat)
        self.t = float(time)

    def __str__(self):
        if self.is_integer:
            return '{:.3f}, {}:{}'.format(self.t, self.bar, round(self.beat))
        else:
            return '{:.3f}, {}:{:.3f}'.format(self.t, self.bar, self.beat)

    def __repr__(self):
        return 'cTime: ' + str(self)

    def __eq__(self, y):
        return super().__eq__(y) and abs(self.t - y.t) < _t_eps

    def get_fTime(self):
        return fTime(self.t)

    def get_bTime(self):
        return bTime(self.bar, self.beat)

    @functools.singledispatchmethod
    def __add__(self, other):
        raise NotImplementedError(other)

    @__add__.register
    def _(self, dur: cDur):
        bb = self.get_bTime() + dur
        t = self.t + dur.secs
        return cTime(bb.bar, bb.beat, t)

    def plus_dur_at_tempo(self, bd: bDur, tempo: Tempo):
        bb = self.get_bTime() + bd
        t = self.t + bd.time_dur(tempo)
        return cTime(bb.bar, bb.beat, t)

    def sub_using_TS(self, t2: 'cTime', ts: TimeSignature):
        b = super().sub_using_TS(t2, ts)
        secs_interval = self.t - t2.t
        return cDur.from_bdur_and_fdur(b, fDur(secs_interval))

    @functools.singledispatchmethod
    def __sub__(self, other):
        raise NotImplementedError(other)


class Frac:
    def __init__(self, I: int, N: int, D: int):
        self.I = I
        self.N = int(N / gcd(N, D))
        self.D = int(D / gcd(N, D))

        if not (isinstance(I, int) and isinstance(N, int) and isinstance(D, int)):
            raise TypeError(
                'I, {}, N {} and D {} must be integers'.format(I, N, D))

        if I < 0 or N < 0 or D < 1 or N >= D:
            print('N/D: {}/{}'.format(N, D))
            raise ValueError(' I {} must be >= 0, N/D: {}/{} must be in [0, 1)'.format(I, N, D))

    def __str__(self):
        if self.N == 0:
            return '{}'.format(self.I)
        else:
            return '{}+{}/{}'.format(self.I, self.N, self.D)

    @classmethod
    def fixed_D(cls, x: float, D: int):
        if x < 0 or D <= 0:
            raise ValueError('x must be >=0, D > 0')

        D = int(D)
        I = floor(x)
        frac_part = x - I
        N = round(frac_part * D)
        if N == D:
            I += 1
            N = 0

        return cls(I, N, D)

    @classmethod
    def D_set(cls, x: float, Dset: List[int] = [1, 2, 3, 4, 8]):
        minD = 0
        minErr = inf
        for d in Dset:
            err = cls.fixed_D(x, d).real() - x
            if abs(err) < minErr:
                minD = d
                minErr = abs(err)

        return cls.fixed_D(x, minD)

    def real(self):
        return self.I + self.N / self.D

    def __rmul__(self, other):
        return self * other

    def __radd__(self, other):
        return self + other

    @functools.singledispatchmethod
    def __sub__(self, other):
        raise NotImplementedError(other)

    @functools.singledispatchmethod
    def __add__(self, other):
        raise NotImplementedError(other)

    @__sub__.register
    def _(self, y: int):
        return Frac(self.I - y, self.N, self.D)

    @__add__.register
    def _(self, y: int):
        return Frac(self.I + y, self.N, self.D)

    @functools.singledispatchmethod
    def __eq__(self, other):
        raise NotImplementedError(other)

    @__eq__.register
    def _(self, other: int):
        return other == self.I and self.N == 0

    @functools.singledispatchmethod
    def __mul__(self, other):
        raise NotImplementedError(other)

    @__mul__.register
    def _(self, other: int):
        if other < 0:
            raise ValueError('Multiplicand {} must be >= 0'.format(other))

        I = self.I * other
        N = self.N * other
        if N >= self.D:
            k = N//self.D
            I += k
            N -= k * self.D

        return Frac(I, N, self.D)

    @functools.singledispatchmethod
    def __truediv__(self, other):
        raise NotImplementedError(other)

    @__truediv__.register
    def _(self, other: int):
        if other <= 0:
            raise ValueError('Divisor {} must be > 0'.format(other))

        N = self.I * self.D + self.N
        D = self.D * other
        I = 0
        if N >= D:
            k = N//D
            I += k
            N -= k * D

        return Frac(I, N, D)

@Frac.__sub__.register
def _(self, y: Frac):
    I = self.I - y.I
    N = self.N * y.D - self.D * y.N
    D = self.D * y.D
    if N >= D:
        N -= D
        I += 1
    if N < 0:
        N += D
        I -= 1
    return Frac(I, N, D)

@Frac.__add__.register
def _(self, y: Frac):
    I = self.I + y.I
    N = self.N * y.D + self.D * y.N
    D = self.D * y.D
    if N >= D:
        N -= D
        I += 1
    return Frac(I, N, D)

@Frac.__mul__.register
def _(self, other: Frac):
    f1 = self * other.I
    f2 = self * other.N
    f2 = f2 / other.D

    return f1 + f2

@Frac.__eq__.register
def _(self, other: Frac):
    return self.I == other.I and self.D * other.N == self.N * other.D

class qDur(cDur):
    def __init__(self, qbeats: Frac, secs: float, ts: TimeSignature):
        super().__init__(qbeats.real(), secs, ts)
        self.qbeats = qbeats

    def __str__(self):
        b = self.qbeats - self.ts.num * self.bars
        beats_str = str(b)

        if self.bars == 0:
            return '{:.3f}, {} beats'.format(self.t, beats_str)
        else:
            return '{:.3f}, {} bars of {}, {}'.format(
                self.t, self.bars, str(self.ts), beats_str)

    @classmethod
    def from_cDur(cls, d: cDur, Dset: List[int] = [1, 2, 3, 4, 8]):
        return cls(Frac.D_set(d.total_beats, Dset), d.secs, d.ts)

    @classmethod
    def from_cDur_and_qbeats(cls, d: cDur, q: Frac):
        return cls(q, d.secs, d.ts)

    # TODO: Tests for this!
    @classmethod
    def from_frac_and_tempo(cls, qbeats: Frac, ts: TimeSignature, tempo: Tempo):
        c = cDur.from_bdur_and_tempo(bDur(qbeats.real(), ts), tempo)
        return cls.from_cDur_and_qbeats(c, qbeats)

    def get_cDur(self):
        return cDur(self.total_beats, self.secs, self.ts)

    @functools.singledispatchmethod
    def __eq__(self, other):
        raise NotImplementedError(other)

    @__eq__.register
    def _(self, other: cDur):
        return super().__eq__(other)

@qDur.__eq__.register
def _(self, other: qDur):
    return super(qDur, self).__eq__(other.get_cDur()) and self.qbeats == other.qbeats


class qTime(cTime):
    def __init__(self, bar: int, qbeat: Frac, time: float):
        super().__init__(bar, qbeat.real(), time)
        self.qbeat = qbeat

    def __str__(self):
        return '{:.3f}, {}:{}'.format(self.t, self.bar, self.qbeat)

    @classmethod
    def from_cTime(cls, t: cTime, ts: TimeSignature, Dset: List[int] = [1, 2, 3, 4, 8]):
        qb = Frac.D_set(t.beat, Dset)
        bar = t.bar
        assert qb.I <= ts.num
        if qb.I == ts.num:
            qb.I = 0
            bar += 1

        return cls(bar, qb, t.t)

    def get_cTime(self):
        return cTime(self.bar, self.beat, self.t)

    @functools.singledispatchmethod
    def __eq__(self, other):
        raise NotImplementedError(other)

    @__eq__.register
    def _(self, other: cTime):
        return super().__eq__(other)

    def sub_using_TS(self, t2: 'qTime', ts: TimeSignature):
        c = super().sub_using_TS(t2, ts)
        qdur = (ts.num * (self.bar - t2.bar) + self.qbeat) - t2.qbeat
        return qDur.from_cDur_and_qbeats(c, qdur)

    @functools.singledispatchmethod
    def __add__(self, other):
        raise NotImplementedError(other)

    @__add__.register
    def _(self, other: qDur):
        cT = self.get_cTime() + other.get_cDur()
        qbeat = self.qbeat + other.qbeats
        if qbeat.I >= other.ts.num:
            qbeat -= other.ts.num

        return qTime(cT.bar, qbeat, cT.t)


    def next_beat(self, ts: TimeSignature, tempo: Tempo):
        if self.qbeat.N == 0:
            bar = deepcopy(self.bar)
            beat = self.qbeat + 1
            if beat.I == ts.num:
                beat.I = 0
                bar += 1

            return qTime(bar, beat, self.t + tempo.beat_time_delta())

        bar = deepcopy(self.bar)
        next_q = deepcopy(self.qbeat)
        next_q.I += 1
        if next_q.I == ts.num:
            next_q.I = 0
            bar += 1

        next_q.N = 0
        next_q.D = 1

        diff_beats = ceil(self.beat) - self.beat
        diff_time = diff_beats * tempo.beat_time_delta()
        return qTime(bar, next_q, self.t + diff_time)

@qTime.__eq__.register
def _(self, other: qTime):
    return super(qTime, self).__eq__(other.get_cTime()) and self.qbeat == other.qbeat


class qStartEnd:
    def __init__(self, start: qTime, end: qTime):
        self.start = start
        self.end = end
        if self.end <= self.start:
            raise ValueError(
                'End {} must be after Start {}'.format(self.end, self.start))

    def dur_at_one_TS(self, ts: TimeSignature):
        return self.end.sub_using_TS(self.start, ts)

    def implied_average_tempo(self, ts: TimeSignature):
        qd = self.dur_at_one_TS(ts)
        t_diff = self.end.t - self.start.t
        beat_delta = t_diff / qd.total_beats
        return Tempo(60.0 / beat_delta, ts.den)

    def get_duration_fragments(self, ts: TimeSignature):
        if self.start.bar == self.end.bar and self.start.qbeat.I == self.end.qbeat.I:
            return [self.dur_at_one_TS(ts)]

        whole_beats = ts.num * (self.end.bar - self.start.bar) + self.end.qbeat.I - self.start.qbeat.I - 1
        if whole_beats < 0:
            raise ValueError('Invalid duration, whole beats == {}'.format(whole_beats))

        tempo = self.implied_average_tempo(ts)
        next_beat = self.start.next_beat(ts, tempo)
        first_bit = next_beat.sub_using_TS(self.start, ts)
        fragments = [first_bit]
        if next_beat == self.end:
            return fragments

        prev_beat = next_beat
        for i in range(0, whole_beats):
            next_beat = prev_beat + qDur.from_frac_and_tempo(Frac(1, 0, 1), ts, tempo)
            fragments.append(next_beat.sub_using_TS(prev_beat, ts))
            prev_beat = next_beat

        if self.end != prev_beat:
            fragments.append(self.end.sub_using_TS(prev_beat, ts))

        return fragments

class Pause:
    def __init__(self, start: cTime, end: cTime):
        if start.get_bTime() != end.get_bTime():
            raise ValueError('Pause must start and end at same beat time, not {} to {}'.format(start, end))
        self.start = start
        self.end = end

    def dur(self):
        return self.end.t - self.start.t

    def __str__(self):
        return 'Pause for {:.3f} seconds at {}'.format(self.dur(), self.start)


class Rit:
    def __init__(self, t1: Tempo, t2: Tempo, bd: bDur):
        self.t1 = t1
        self.t2 = t2
        self.bd = bd

    def __str__(self):
        return 'Tempo change from {} to {}, lasting {}'.format(
            self.t1, self.t2, self.bd)

    def cDur_after_offset(self, dur: bDur):
        if dur.total_beats < 0 or dur > self.bd:
            return None

        d1 = self.t1.semibreve_time_delta()
        d2 = self.t2.semibreve_time_delta()
        D = self.bd.semibreves_dur()
        offset = dur.semibreves_dur()

        if self.t1 == self.t2 or abs(d1 - d2) < _t_eps:
            return cDur.from_bdur_and_tempo(dur, self.t1)

        delta = fDur(1.0 * D * d1 * d2 / (d1 - d2) *
                     log(1.0 - (d2 - d1) / (d2 * D) * offset))

        return cDur.from_bdur_and_fdur(dur, delta)

    def total_cDur(self):
        return self.cDur_after_offset(self.bd)


class FixedRit(Rit):
    def __init__(self, start: cTime, t1: Tempo, t2: Tempo, bd: bDur):
        super().__init__(t1, t2, bd)
        self.start = start

    @classmethod
    def from_time_and_rit(cls, t: cTime, rit: Rit):
        return cls(t, rit.t1, rit.t2, rit.bd)

    def end(self):
        return self.start + self.total_cDur()

    def time_at_BBtime(self, bb: bTime):
        offset = bb.sub_using_TS(self.start, self.bd.ts)
        return self.start + self.cDur_after_offset(offset)

    def __str__(self):
        return '{}, starting at {}'.format(super().__str__(), self.start)
