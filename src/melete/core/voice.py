from melete.core.note import qNote, cNote, cTime, qTime
from melete.core.timeframe import TimeFrame, StructuredTimeFrame, NoteOverlapError

from typing import List

class cVoice:
    def __init__(self, start: cTime, notes: List[cNote], tf: TimeFrame):
        self.start = start
        self.notes = notes
        self.tf = tf


    def __str__(self):
        return '{} notes, from start {}, in timeframe {}'.format(len(self.notes), self.start, self.tf)


class qVoice(cVoice):
    def __init__(self, start: qTime, notes: List[qNote], tf: TimeFrame):
        super().__init__(start, notes, tf)


class structuredqVoice(qVoice):
    def __init__(self, start: qTime, notes: List[qNote], tf: StructuredTimeFrame):
        super().__init__(start, notes, tf)
        # TODO: Sort notes?
        self.add_missing_rests_and_fix_overlap()


    def add_missing_rests_and_fix_overlap(self):
        if self.notes[0].start > self.start:
            self.notes.insert(0, qNote(None, self.start, self.notes[0].start))

        i = 0
        while i + 1 < len(self.notes):
            try:
                gap = self.tf.get_gap(self.notes[i], self.notes[i + 1])
                if gap is not None:
                    self.notes.insert(i + 1, qNote(None, gap.start, gap.end))
                    i += 1

            except NoteOverlapError as err:
                print(err.message)
                self.notes[i].end = (self.notes[i + 1].start)

            i += 1

