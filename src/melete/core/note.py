from melete.core.mtime import cTime, cDur, fTime, fDur, bTime, bDur, qTime, qStartEnd, TimeSignature
from melete.core.pitch import Pitch

class Note:
    def __init__(self, pitch: Pitch, start, end):
        self.pitch = pitch
        self.start = start
        self.end = end

    def __str__(self):
        if self.pitch is None:
            return 'Rest, from {} to {}'.format(
                self.start, self.end)
        else:
            return '{}, from {} to {}'.format(
                str(self.pitch), self.start, self.end)

class fNote(Note):
    def __init__(self, pitch: Pitch, start: fTime, end: fTime):
        super().__init__(pitch, start, end)


class bNote(Note):
    def __init__(self, pitch: Pitch, start: bTime, end: bTime):
        super().__init__(pitch, start, end)


class cNote(bNote):
    def __init__(self, pitch: Pitch, start: cTime, end: cTime):
        super().__init__(pitch, start, end)

class qNote(cNote):
    def __init__(self, pitch: Pitch, start: qTime, end: qTime):
        super().__init__(pitch, start, end)

    @classmethod
    def from_qstartend(cls, pitch: Pitch, qse: qStartEnd):
        return cls(pitch, qse.start, qse.end)

    def qse(self):
        return qStartEnd(self.start, self.end())

    def get_duration_fragments(self, ts: TimeSignature):
        return self.qse().get_duration_fragments(ts)
