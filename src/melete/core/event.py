from enum import Enum

class EventType(Enum):
    TS = 0
    tempo = 1
    rit = 2
    clicks = 3
    cbp = 4
    pause = 5

class EventHolder:
    def __init__(self, time, event, eventType):
        self.t = time # Should be a cTime
        self.e = event
        self.type = eventType

    def __str__(self):
        return 'At {}: {}'.format(self.t, self.e)

